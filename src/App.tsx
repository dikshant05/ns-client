import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './design-css/NS-Design.css';
import './design-css/Menu.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Franchise from './pages/franchise';
import Offers from './pages/offers';
import Dealership from './pages/dealership';
import VehicleModel from './pages/vehicleModel';
import VehicleVersion from './pages/vehicleVersion';
import Interior from './pages/interior';
import Exterior from './pages/exterior';
import ArchivedDocs from './pages/archivedDocs';
import PerformanceReports from './pages/performanceReports';
import HelpDocs from './pages/helpDocs';
import Feedback from './pages/feedback';
import Settings from './pages/settings';
import Template from './pages/template';
import DynamicCategories from './pages/dynamicCategories';
import SystemUser from './pages/systemUser';
import MarketUser from './pages/marketUser';
import SystemRoles from './pages/systemRoles';
import MarketRoles from './pages/marketRoles';
import ParseAuth from './pages/parseAuth';
import { ProtectedRoute } from './utility/protectedRoute';

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Routes>
          <Route path={'/'} element={(<ProtectedRoute><Franchise /></ProtectedRoute>)} />
          <Route path={'/parseAuth'} element={<ParseAuth />} />
          <Route path={'/franchise'} element={(<ProtectedRoute><Franchise /></ProtectedRoute>)} />
          <Route path={'/offers'} element={(<ProtectedRoute><Offers /></ProtectedRoute>)} />
          <Route path={'/dealership'} element={(<ProtectedRoute><Dealership /></ProtectedRoute>)} />
          <Route path={'/vehiclemodel'} element={(<ProtectedRoute><VehicleModel /></ProtectedRoute>)} />
          <Route path={'/vehicleversion'} element={(<ProtectedRoute><VehicleVersion /></ProtectedRoute>)} />
          <Route path={'/interior'} element={(<ProtectedRoute><Interior /></ProtectedRoute>)} />
          <Route path={'/exterior'} element={(<ProtectedRoute><Exterior /></ProtectedRoute>)} />
          <Route path={'/archiveddocs'} element={(<ProtectedRoute><ArchivedDocs /></ProtectedRoute>)} />
          <Route path={'/performancereports'} element={(<ProtectedRoute><PerformanceReports /></ProtectedRoute>)} />
          <Route path={'/helpdocs'} element={(<ProtectedRoute><HelpDocs /></ProtectedRoute>)} />
          <Route path={'/feedback'} element={(<ProtectedRoute><Feedback /></ProtectedRoute>)} />
          <Route path={'/settings'} element={(<ProtectedRoute><Settings /></ProtectedRoute>)} />
          <Route path={'/template'} element={(<ProtectedRoute><Template /></ProtectedRoute>)} />
          <Route path={'/dynamiccategories'} element={(<ProtectedRoute><DynamicCategories /></ProtectedRoute>)} />
          <Route path={'/systemuser'} element={(<ProtectedRoute><SystemUser /></ProtectedRoute>)} />
          <Route path={'/marketuser'} element={(<ProtectedRoute><MarketUser /></ProtectedRoute>)} />
          <Route path={'/systemroles'} element={(<ProtectedRoute><SystemRoles /></ProtectedRoute>)} />
          <Route path={'/marketroles'} element={(<ProtectedRoute><MarketRoles /></ProtectedRoute>)} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
