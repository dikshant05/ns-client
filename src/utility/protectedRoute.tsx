import { useState } from 'react';
import { Config } from '../config/config';
import { CheckSession } from '../services/parseAuth';

export const ProtectedRoute = ({ children }: { children: JSX.Element }) => {
    const [session, setSession] = useState(1)
    CheckSession().then(data => setSession(data)).catch(err => setSession(0))
    if(session === 1){ 
        return children
    }else{
        window.location.replace(Config.AuthorizationUrl); 
        return null
    }
};