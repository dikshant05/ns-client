import React from 'react';
import '../design-css/Table.css'

const TableSection = (props:any) => {
    return(
        <div className={`${props.width} table-section`}>
            <table className='table'>
                <thead>
                    <tr>
                        {props.rowData.map((item:any, i:number) => (
                            <th key={i}>{item}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {(props.bodyData && props.bodyData.length > 0) ? props.bodyData.map((item:any, i:number) => (
                            <th key={i}>{item}</th>
                        ))
                        :<td colSpan={props.rowData.length}>No Data Found</td>}
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default TableSection