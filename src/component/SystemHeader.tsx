import React, { FunctionComponent } from 'react';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faDownload, faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const SystemHeader:FunctionComponent = () => {
    return(
        <div className='w-100 content-section'>
            <p>Vehicle Model</p>
            <div className='search-product'>
                <input type={'text'} name='psrch' className='form-control' placeholder='Search Item..' />
                <button><FontAwesomeIcon icon={faSearch} color={'#000'} /></button>
            </div>
            <div>
                <FontAwesomeIcon icon={faDownload} color={'#000'} style={{marginRight:'30px'}} />
                <FontAwesomeIcon icon={faPlus} color={'#000'} style={{marginRight:'30px'}} />
                <FontAwesomeIcon icon={faUser} color={'#000'} style={{marginRight:'30px'}} />
            </div>
        </div>
    )
}

export default SystemHeader