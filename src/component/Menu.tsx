import { faFolder } from '@fortawesome/free-regular-svg-icons';
import { faGear, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FunctionComponent } from 'react';
import { Accordion, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Logo from '../assets/nissan-logo-2020-white-min.png'

const Menu:FunctionComponent = () => {
    const renderTooltip = (props:any) => (
        <Tooltip id="button-tooltip" {...props}>
          Manage System
        </Tooltip>
    );
    const renderTooltipContent = (props:any) => (
        <Tooltip id="button-tooltip" {...props}>
          Manage Content
        </Tooltip>
    );
    const renderTooltipUsers = (props:any) => (
        <Tooltip id="button-tooltip" {...props}>
          Manage Users
        </Tooltip>
    );
    return(
        <div className='col-md-1 p-relative'>
            <div className={'Menu_Section'}>
                <img src={Logo} alt={'logo'} />
                <Accordion className={'list-item'}>
                    <Accordion.Item className={'list-itemli'} eventKey="0">
                        <Accordion.Header><OverlayTrigger placement="right" overlay={renderTooltip}><FontAwesomeIcon icon={faGear} color={'#fff'} /></OverlayTrigger></Accordion.Header>
                        <Accordion.Body>
                        
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header><OverlayTrigger placement="right" overlay={renderTooltipContent}><FontAwesomeIcon icon={faFolder} color={'#fff'} /></OverlayTrigger></Accordion.Header>
                        <Accordion.Body>
                        
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <Accordion.Header><OverlayTrigger placement="right" overlay={renderTooltipUsers}><FontAwesomeIcon icon={faUsers} color={'#fff'} /></OverlayTrigger></Accordion.Header>
                        <Accordion.Body>
                        
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </div>
        </div>
    )
}

export default Menu