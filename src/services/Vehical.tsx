import axios from 'axios';
import {Config} from '../config/config'

//Model API
export const NSR_Models_Search = async () => {
    let ns_url = `${Config.endpoint}/vehicles/models/search`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Models_Fetch = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/models?id=${id}`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Models_Create = async () => {
    let ns_url = `${Config.endpoint}/vehicles/models/create`
    const create_list = await axios.post(ns_url)
    return create_list.data
}

export const NSR_Models_Update = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/models/update`
    let data = {id: id}
    const updt_list = await axios.patch(ns_url, data)
    return updt_list.data
}

export const NSR_Models_Enable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/models/enable`
    let data = {id: id}
    const enbl_list = await axios.patch(ns_url, data)
    return enbl_list.data
}

export const NSR_Models_Disable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/models/disable`
    let data = {id: id}
    const disbl_list = await axios.patch(ns_url, data)
    return disbl_list.data
}

export const NSR_Models_UploadURL = async () => {
    let ns_url = `${Config.endpoint}/vehicles/models/uploadurl`
    const disbl_list = await axios.patch(ns_url)
    return disbl_list.data
}

export const NSR_Models_DeleteImg = async () => {
    let ns_url = `${Config.endpoint}/vehicles/models/deleteimg`
    const disbl_list = await axios.delete(ns_url)
    return disbl_list.data
}

// Model Version API
export const NSR_Version_Search = async () => {
    let ns_url = `${Config.endpoint}/vehicles/versions/search`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Version_Fetch = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/versions?id=${id}`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Version_Create = async () => {
    let ns_url = `${Config.endpoint}/vehicles/versions/create`
    const create_list = await axios.post(ns_url)
    return create_list.data
}

export const NSR_Version_Enable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/versions/enable`
    let data = {id: id}
    const enbl_list = await axios.patch(ns_url, data)
    return enbl_list.data
}

export const NSR_Version_Disable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/versions/disable`
    let data = {id: id}
    const disbl_list = await axios.patch(ns_url, data)
    return disbl_list.data
}

export const NSR_Version_Get = async () => {
    let ns_url = `${Config.endpoint}/vehicles/versions/list`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}

export const NSR_Version_ByModel = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/versions/bymodel?id=${id}`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}

// Interior Colour API
export const NSR_Interior_Search = async () => {
    let ns_url = `${Config.endpoint}/vehicles/interior/search`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Interior_Fetch = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/interior?id=${id}`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Interior_Create = async () => {
    let ns_url = `${Config.endpoint}/vehicles/interior/create`
    const create_list = await axios.post(ns_url)
    return create_list.data
}

export const NSR_Interior_Enable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/interior/enable`
    let data = {id: id}
    const enbl_list = await axios.patch(ns_url, data)
    return enbl_list.data
}

export const NSR_Interior_Disable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/interior/disable`
    let data = {id: id}
    const disbl_list = await axios.patch(ns_url, data)
    return disbl_list.data
}

export const NSR_Interior_Get = async () => {
    let ns_url = `${Config.endpoint}/vehicles/interior/list`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}

export const NSR_Interior_ByVersion = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/interior/byversion?id=${id}`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}

// Exterior Colour API
export const NSR_Exterior_Search = async () => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/search`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Exterior_Fetch = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors?id=${id}`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Exterior_Create = async () => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/create`
    const create_list = await axios.post(ns_url)
    return create_list.data
}

export const NSR_Exterior_Enable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/enable`
    let data = {id: id}
    const enbl_list = await axios.patch(ns_url, data)
    return enbl_list.data
}

export const NSR_Exterior_Disable = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/disable`
    let data = {id: id}
    const disbl_list = await axios.patch(ns_url, data)
    return disbl_list.data
}

export const NSR_Exterior_Get = async () => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/list`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}

export const NSR_Exterior_ByVersion = async (id:any) => {
    let ns_url = `${Config.endpoint}/vehicles/exteriors/byversion?id=${id}`
    const disbl_list = await axios.get(ns_url)
    return disbl_list.data
}