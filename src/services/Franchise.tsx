import axios from 'axios';
import {Config} from '../config/config'

export const NSR_Fetch = async () => {
    let ns_url = `${Config.endpoint}/franchise/list`
    const fetch_list = await axios.get(ns_url)
    return fetch_list.data
}

export const NSR_Create = async () => {
    let ns_url = `${Config.endpoint}/franchise/create`
    const create_list = await axios.post(ns_url)
    return create_list.data
}

export const NSR_Update = async (id:any) => {
    let ns_url = `${Config.endpoint}/franchise/update?id=${id}`
    const updt_list = await axios.patch(ns_url)
    return updt_list.data
}

export const NSR_Enable = async (id:any) => {
    let ns_url = `${Config.endpoint}/franchise/enable?id=${id}`
    const enbl_list = await axios.patch(ns_url)
    return enbl_list.data
}

export const NSR_Disable = async (id:any) => {
    let ns_url = `${Config.endpoint}/franchise/disable?id=${id}`
    const disbl_list = await axios.patch(ns_url)
    return disbl_list.data
}
