import axios from 'axios';
import {Config} from '../config/config'

export const NSR_AWSCodeExchange = async (data:any) => {
    let ns_url = `${Config.endpoint}/api/parseauth`
    const response = await axios.post(ns_url, data)
    return response.data
}

export const WithAuthorizationHeaders = (headers = {}) => {
    try {
        const loggedInUser = localStorage.getItem('aws_key')
        if (loggedInUser && loggedInUser.length > 0) {
            return { ...headers, 'x-access-token': loggedInUser }
        }
    } catch (error) {
        console.debug('Failed to generate auth headers')
    }
    throw new Error('Invalid authorization')
}

export const CheckSession = async () => {
    const aws_token = localStorage.getItem('aws_key')
    const sf_token = localStorage.getItem('sf_key')
    if(aws_token && sf_token){
        let ns_url = `${Config.endpoint}/api/authtoken`
        const fetch_list = await axios.get(ns_url, {headers: WithAuthorizationHeaders()})
        if(fetch_list.data.message && fetch_list.data.message['custom:access_token'] === sf_token){
            return 1
        }else{
            return 0
        }
    }else{
        return 0
    }
}

export const AddAuthDatatoLocal = (aws_token:string, sf_token:string) => {
    localStorage.setItem('aws_key', aws_token)
    localStorage.setItem('sf_key', sf_token)
}

export const RemoveAuthDatafromLocal = () => {
    localStorage.clear()
}