import React, { FunctionComponent } from 'react';
import Menu from '../component/Menu';
import SystemHeader from '../component/SystemHeader';
import TableSection from '../component/Table';

const VehicleModel:FunctionComponent = () => {
    const trData:any = ['Model Code', 'Model Name', 'Region', 'Thumbnail', 'Status']
    const tdData:any = []
    return(
        <div className='container web_width'>
            <div className='row'>
                <Menu />
                <div className='col-md-11 pad-10'>
                    <SystemHeader />
                    <TableSection width={'w-70'} rowData={trData} bodyData={tdData} />
                </div>
            </div>
        </div>
    )
}

export default VehicleModel