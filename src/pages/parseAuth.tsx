import React, { FunctionComponent, useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { AddAuthDatatoLocal, NSR_AWSCodeExchange } from '../services/parseAuth';

const ParseAuth:FunctionComponent = () => {
    let navigate = useNavigate();
    const [searchParams] = useSearchParams();
    const [responseMsg, setresponseMsg] = useState('Please wait while we redirecting..')
    const extract_code = searchParams.get("code")?.slice(0)
    useEffect(() => {
        if(extract_code){
            let data = { code : extract_code}
            NSR_AWSCodeExchange(data).then(data => {
                if(data.id_token) { 
                    AddAuthDatatoLocal(data.access_token, data.sf_token)
                    setresponseMsg('Please wait while we redirecting..')
                    return navigate("/franchise");
                }else{ 
                    setresponseMsg('Unauthorized Access.')
                }
            }).catch(err => console.log(err.message))
        }else{
            setresponseMsg('Unauthorized Access.')
        }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps
    return(
        <div>
            <h4>{responseMsg}</h4>
        </div>
    )
}

export default ParseAuth