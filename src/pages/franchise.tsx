import React, { FunctionComponent } from 'react';
import { Config } from '../config/config';
import { RemoveAuthDatafromLocal } from '../services/parseAuth';

const Franchise:FunctionComponent = () => {
    const Usrloggout = (e:any) => {
        RemoveAuthDatafromLocal()
        window.location.href = Config.LogoutUrl
    }
    return(
        <div>
            <h1>Franchise</h1>
            <button onClick={(e) => Usrloggout(e)}>Logout</button>
        </div>
    )
}

export default Franchise