export const Config = {
    endpoint: process.env.REACT_APP_NS_SERVER,
    AuthorizationUrl: `${process.env.REACT_APP_AUTH_SECRET_URI}?client_id=${process.env.REACT_APP_AWS_COGNITO_CLIENT_ID}&response_type=code&scope=openid&redirect_uri=${process.env.REACT_APP_AWS_COGNITO_REDIRECT_URI}`,
    LogoutUrl: `${process.env.REACT_APP_AUTH_LOGOUT_URI}?client_id=${process.env.REACT_APP_AWS_COGNITO_CLIENT_ID}&logout_uri=${process.env.REACT_APP_SF_LOGOUT_URI}`
}